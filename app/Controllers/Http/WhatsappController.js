"use strict";

class WhatsappController {
  async Whatsapp({ request, response }) {
    const data_request = request.only(["numero", "foto", "visto", "operadora"]);
    const urls = [
      "Whatsapp_01",
      "Whatsapp_02",
      "Whatsapp_03",
      "Whatsapp_04",
      "Whatsapp_05",
    ];
    const api = urls[(Math.random() * urls.length) | 0];
    if (api == "Whatsapp_01") {
      var client = global.Whatsapp_01;
    } else if (api == "Whatsapp_02") {
      var client = global.Whatsapp_02;
    } else if (api == "Whatsapp_03") {
      var client = global.Whatsapp_03;
    } else if (api == "Whatsapp_04") {
      var client = global.Whatsapp_04;
    } else if (api == "Whatsapp_05") {
      var client = global.Whatsapp_05;
    }

    const checkNumberStatus = await client.checkNumberStatus(
      data_request.numero + "@c.us"
    );

    if (checkNumberStatus.status == "404") {
      const data = {
        receber: false,
        business: false,
        existe: false,
        foto: false,
        visto: false,
        operadora: false,
        failed: false,
      };
      return response.status(200).send({ data });
    } else if (checkNumberStatus.status == "200") {
      if (data_request.foto) {
        var getProfilePicFromServer = await client.getProfilePicFromServer(
          checkNumberStatus.id._serialized
        );
      } else {
        getProfilePicFromServer = false;
      }
      if (data_request.visto) {
        var getLastSeen = await client.getLastSeen(
          checkNumberStatus.id._serialized
        );
      } else {
        getLastSeen = false;
      }

      if (data_request.operadora) {
        var operadora = "VIVO";
      } else {
        operadora = false;
      }

      const data = {
        receber: checkNumberStatus.canReceiveMessage,
        business: checkNumberStatus.isBusiness,
        existe: checkNumberStatus.numberExists,
        foto: getProfilePicFromServer,
        visto: getLastSeen,
        operadora: operadora,
        failed: false,
      };

      return response.status(200).send({ data });
    } else {
      const data = {
        failed: true,
      };

      return response.status(200).send({ data });
    }
  }
}
module.exports = WhatsappController;
