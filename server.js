"use strict";

/*
|--------------------------------------------------------------------------
| Http server
|--------------------------------------------------------------------------
|
| This file bootstraps Adonisjs to start the HTTP server. You are free to
| customize the process of booting the http server.
|
| """ Loading ace commands """
|     At times you may want to load ace commands when starting the HTTP server.
|     Same can be done by chaining `loadCommands()` method after
|
| """ Preloading files """
|     Also you can preload files by calling `preLoad('path/to/file')` method.
|     Make sure to pass a relative path from the project root.
*/

const { Ignitor } = require("@adonisjs/ignitor");

new Ignitor(require("@adonisjs/fold"))
  .appRoot(__dirname)
  .fireHttpServer()
  .catch(console.error);

const venom = require("venom-bot");

const options = {
  logQR: false,
  disableSpins: true,
  useChrome: false,
  disableWelcome: false,
  autoClose: false,
};

venom
  .create(
    "Whatsapp_01",
    (asciiQR) => {
      console.log(asciiQR);
    },
    undefined,
    {
      options,
    }
  )
  .then((client) => {
    global.Whatsapp_01 = client;
  })
  .catch((error) => console.log(error));

venom
  .create(
    "Whatsapp_02",
    (asciiQR) => {
      console.log(asciiQR);
    },
    undefined,
    {
      options,
    }
  )
  .then((client) => {
    global.Whatsapp_02 = client;
  })
  .catch((error) => console.log(error));

venom
  .create(
    "Whatsapp_03",
    (asciiQR) => {
      console.log(asciiQR);
    },
    undefined,
    {
      options,
    }
  )
  .then((client) => {
    global.Whatsapp_03 = client;
  })
  .catch((error) => console.log(error));

venom
  .create(
    "Whatsapp_04",
    (asciiQR) => {
      console.log(asciiQR);
    },
    undefined,
    {
      options,
    }
  )
  .then((client) => {
    global.Whatsapp_04 = client;
  })
  .catch((error) => console.log(error));

venom
  .create(
    "Whatsapp_05",
    (asciiQR) => {
      console.log(asciiQR);
    },
    undefined,
    {
      options,
    }
  )
  .then((client) => {
    global.Whatsapp_05 = client;
  })
  .catch((error) => console.log(error));
